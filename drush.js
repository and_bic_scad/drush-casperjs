/**
 * CasperJS Drush helpers.
 */

/**
 * Execute a drush command.
 *
 * @param command
 *   The drush command to execute. For simple commands that require no arguments
 *   this can be a string. For complex commands that require arguments this
 *   should be an array where each element is a part of the command. e.g.)
 *   ['pm-info', 'overlay', '--yes'].
 *
 * @returns mixed
 *   Either the output to stdOut from the drush command or false if the
 *   command failed.
 */
casper.drush = function(command) {
  var spawn;

  // Make sure we can load the rquired child_process library from PhantomJS.
  try {
    spawn = require("child_process").spawn;
  } catch (e) {
    this.log(e, "error");
  }

  if (spawn) {
    var is_finished = false;
    // Grab the drush alias to use when executing this command from the options
    // passed to the casper script at initialization.
    var alias = casper.cli.get('drush-alias');
    var output;

    // Prepare our command by making sure it's in the appropriate format for the
    // spawn executor.
    if (typeof command === 'object') {
      command = [alias].concat(command);
    }
    else {
      command = [alias, command];
    }

    this.log('Running drush command: ' + command.join(' '));

    var child = spawn('drush', command);

    child.stdout.on("data", function (data) {
      casper.log('drush command success');
      output = data;
    })

    child.stderr.on("data", function (data) {
      // There was an error executing the command so just return false.
      casper.log('drush command error');
      output = data;
    })

    child.on("exit", function (code) {
      // Finished the drush command.
      is_finished = true;
    });

    while (!is_finished) {
      // Do nothing, just block and wait for drush to do it's thing.
    }

    this.log('drush command output: ' + output);
    return output;
  }

  return false;
};
